import { Component } from '@angular/core';
import { FirebaseX } from '@awesome-cordova-plugins/firebase-x/ngx';
import { MamService } from '../mam.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  deviceId : string = null;
  platform : string = null;
  constructor(private firebasex : FirebaseX,
    private mamHelper: MamService) {
      this.deviceId = mamHelper.deviceId;
      this.platform = mamHelper.platform;
      console.log(this.deviceId);
      this.firebasex.setScreenName("Tab3 Page");
      
      
    }
    ionViewDidEnter() {
      window["BOOMR"].addVar({
        "user_ip": "user_ip",
        "user_id": this.deviceId,
        "platform": this.platform,
        "t_name": "tab3_txn",
        "ssnId": "session_id"
     });
    }

    public async logEvent(): Promise<void> {
      this.firebasex.logEvent("select_content", {content_type: "page_view", item_id: "home"});
      
    }
    public async setUP(): Promise<void> {
      this.firebasex.setUserProperty("Platform",this.mamHelper.platform);
      this.firebasex.setUserProperty("DeviceId",this.mamHelper.deviceId)
    }

}
