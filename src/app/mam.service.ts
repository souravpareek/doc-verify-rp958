import { Injectable } from '@angular/core';
import { Device } from '@awesome-cordova-plugins/device/ngx';
import { FirebaseX } from '@awesome-cordova-plugins/firebase-x/ngx';
import { initializeApp } from "firebase/app";
import { getPerformance, trace } from "firebase/performance";

@Injectable({
  providedIn: 'root'
})
export class MamService {
  deviceId : string = null;
  platform : string = null;
  constructor(private firebaseX: FirebaseX,
    private device: Device) {
      this.deviceId = this.device.uuid;
      if(this.deviceId == null) this.deviceId = this.device.serial;
      this.platform = this.device.platform;
      
     }
  public async initMam(){
    this.firebaseX.setCrashlyticsCollectionEnabled(true);
    this.firebaseX.setAnalyticsCollectionEnabled(true);
    this.firebaseX.setPerformanceCollectionEnabled(true);
          
    this.firebaseX.setUserId(this.deviceId);
    this.firebaseX.setCrashlyticsUserId(this.deviceId);
    this.firebaseX.startTrace("userId: "+this.deviceId);
    this.firebaseX.stopTrace("userId: "+this.deviceId);
          const firebaseConfig = {
              apiKey: "AIzaSyBOUWUSVRc--qQRYvTTsvjo2LQbE_hygQs",
              authDomain: "heal-mam-test-project.firebaseapp.com",
              databaseURL: "https://heal-mam-test-project-default-rtdb.asia-southeast1.firebasedatabase.app",
              projectId: "heal-mam-test-project",
              storageBucket: "heal-mam-test-project.appspot.com",
              messagingSenderId: "917117469880",
              appId: "1:917117469880:web:31235f9577e4482949e04d",
              measurementId: "G-N2HV0NW3K6"
            };
            const app = initializeApp(firebaseConfig);
            const perf = getPerformance(app);
            const t = trace(perf, "userIdTrace");
            if(null != this.device.uuid){
              t.putAttribute("userId", this.deviceId.toString());
            }
            t.start();
            t.stop();
            window["BOOMR"].addVar({
              "user_ip": "user_ip",
              "user_id": this.deviceId,
              "platform": this.platform,
              "t_name": "MAM-HELPER",
              "ssnId": "session_id"
           });
  }
     
}
