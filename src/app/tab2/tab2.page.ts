import { Component } from '@angular/core';
import { FirebaseX } from '@awesome-cordova-plugins/firebase-x/ngx';
import { MamService } from '../mam.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  deviceId : string = null;
  platform : string = null;
  constructor(private firebasex : FirebaseX,
    private mamHelper: MamService) {
      this.deviceId = mamHelper.deviceId;
      this.platform = mamHelper.platform;
      console.log(this.deviceId);
      this.firebasex.setScreenName("Tab2 Page");
      
      
    }

    ionViewDidEnter() {
      window["BOOMR"].addVar({
        "user_ip": "user_ip",
        "user_id": this.deviceId,
        "platform": this.platform,
        "t_name": "tab2_txn",
        "ssnId": "session_id"
     }); 
    }
    public async st(): Promise<void> {
      this.firebasex.startTrace("Performance Trac");
      
    }
    public async stt(): Promise<void> {
      this.firebasex.stopTrace("Performance Trace");
    }
  
    public async im(): Promise<void> {
      this.firebasex.incrementCounter("Performance Trace");
    }
}
