import { Component } from '@angular/core';
import { FirebaseX } from '@awesome-cordova-plugins/firebase-x/ngx';
import * as StackTrace from 'stacktrace-js';
import { MamService } from '../mam.service';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  
  deviceId : string = null;
  platform : string = null;
  constructor(private firebasex : FirebaseX, 
    private mamHelper: MamService) {
      mamHelper.initMam();
      this.deviceId = mamHelper.deviceId;
      this.platform = mamHelper.platform;
      this.firebasex.setScreenName("Tab1 Page");
      window["BOOMR"].autorun = false;
      //BOOMR_config.autorun = false;
      window["BOOMR"].History = {enabled : true};

          
    }
    ionViewDidEnter() {
      window["BOOMR"].addVar({
        "user_ip": "user_ip",
        "user_id": this.deviceId,
        "platform": this.platform,
        "t_name": "tab1_txn",
        "ssnId": "session_id"
     }); 
    }
    
    public async crash(): Promise<void> {
      //this.firebasex.setCrashlyticsUserId(this.deviceId);
      this.firebasex.sendCrash();
    }
  
    public async anr(): Promise<void> {
      
    }
    public async nonFatal(): Promise<void> {
      var appRootLevel = window.location.href.replace("index.html","");
      window.onerror = function(errorMsg, url, line, col, error) {
        var logMessage = errorMsg.toString();
        var stackTrace = null;
        var sendError = function() {
          this.firebasex.logError(logMessage, stackTrace, function() {
            console.log("Sent JS Exception");
          }, function(error){
            console.error("Unable to send JS Exception", error);
          });
        };
        logMessage += ': url='+url.replace(appRootLevel, '')+'; line'+line+'; col='+col;
        if(typeof error === 'object')
        {
          StackTrace.fromError(error).then(function(trace) {
            stackTrace = trace;
            sendError()
          });
        }else sendError();
      };
      var st = console.trace;
      //this.firebasex.setCrashlyticsUserId(this.deviceId);
      this.firebasex.logError("TestException", st);
    }
  
}
