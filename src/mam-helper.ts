import { FirebaseX } from '@awesome-cordova-plugins/firebase-x/ngx';
import { Device } from '@awesome-cordova-plugins/device/ngx';
import { initializeApp } from "firebase/app";
import { getPerformance, trace } from "firebase/performance";

// export declare function initialize() : Helper;
export class Helper {
    constructor(private firebaseX: FirebaseX, 
        private device: Device, 
        public deviceId: String) { 
            this.firebaseX.setCrashlyticsCollectionEnabled(true);
            this.firebaseX.setAnalyticsCollectionEnabled(true);
            this.firebaseX.setPerformanceCollectionEnabled(true);
            var y:number;
            var platform = this.device.platform;
            if(platform === 'android')
            {
                var version = this.device.version;
                y = +version;
            }
            if(platform === 'android' && y < 8)
            {
                deviceId = this.device.serial;
            }
            else
            {
                deviceId = this.device.uuid;
            }
            this.firebaseX.setUserId(this.deviceId.toString());
            this.firebaseX.setCrashlyticsUserId(this.deviceId.toString());
            const firebaseConfig = {
                apiKey: "AIzaSyBOUWUSVRc--qQRYvTTsvjo2LQbE_hygQs",
                authDomain: "heal-mam-test-project.firebaseapp.com",
                databaseURL: "https://heal-mam-test-project-default-rtdb.asia-southeast1.firebasedatabase.app",
                projectId: "heal-mam-test-project",
                storageBucket: "heal-mam-test-project.appspot.com",
                messagingSenderId: "917117469880",
                appId: "1:917117469880:web:31235f9577e4482949e04d",
                measurementId: "G-N2HV0NW3K6"
              };
              const app = initializeApp(firebaseConfig);
              const perf = getPerformance(app);
              const t = trace(perf, "userIdTrace");
              if(null != this.device.uuid){
                t.putAttribute("userId", this.deviceId.toString());
              }
              t.start();
              t.stop();
              window["BOOMR"].addVar({
                "user_ip": "user_ip",
                "user_id": this.deviceId.toString(),
                "platform": platform,
                "t_name": "MAM-HELPER",
                "ssnId": "session_id"
             });
        }
   
    
    getDeviceId() : string{
        return this.deviceId.toString();
    }
    

}